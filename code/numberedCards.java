package code;

public class numberedCards implements UnoCard {
    protected int num;
    protected String color;

    public numberedCards(int num, String color) {
        this.num = num;
        this.color = color;
    }

    public int getNum() {
        return this.num;
    }

    @Override
    public String getColor() {
        return this.color;
    }

    @Override
    public boolean canPlay(UnoCard c) {

        if (c instanceof numberedCards && this.getNum() == ((numberedCards) c).getNum()
                || this.getColor() == c.getColor() || c instanceof specialCards) {
            return true;
        } else
            return false;
    }

}
