package code;

import java.util.ArrayList;
import java.util.Collections;

public class deck {
    ArrayList<UnoCard> cards;

    public deck() {
        cards = new ArrayList<UnoCard>();
    }

    public void addToDeck(UnoCard c) {
        cards.add(c);
    }

    public UnoCard draw(ArrayList<UnoCard> c) {
        UnoCard top = c.get(0);
        c.remove(0);
        return top;
    }

    public void shuffle(ArrayList<UnoCard> c) {
        Collections.shuffle(c);
    }
}
