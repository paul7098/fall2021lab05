package code;

public abstract class specialCards implements UnoCard {
    protected String color;

    public specialCards(String color) {
        this.color = color;
    }

    public abstract void performAction();

    public boolean canPlay(UnoCard c) {
        if (this.getColor() == c.getColor() || c instanceof wildCard) {
            return true;
        } else
            return false;
    }
}
