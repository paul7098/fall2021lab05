package code;

interface UnoCard {
    boolean canPlay(UnoCard c);

    String getColor();
}
